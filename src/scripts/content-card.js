;(function ($, window, document, undefined) {

    'use strict';

    var pluginName = 'contentCard',
        defaults = {
            openBtnClassName: 'js-open',
            deleteBtnClassName: 'js-delete',
            printBtnClassName: 'js-print'
        };

        function Plugin (element, options) {

            this.element = element;
            this.settings = $.extend({}, defaults, options);

            this._name = pluginName;
            this._defaults = defaults;

            this.$el = $(this.element);

            this.init();

        }

        $.extend(Plugin.prototype, {

            init: function () {

                this.setupEventListeners();

            },

            setupEventListeners: function () {

                this.$el.on('click', '.' + this.settings.openBtnClassName, function (e) {
                    e.preventDefault();
                    alert('Let\'s say that I\'m open now! :)');
                });

                this.$el.on('click', '.' + this.settings.deleteBtnClassName, function (e) {
                    e.preventDefault();
                    this.$el.remove();
                    console.log('This one actualy works :) Woohoo!');
                }.bind(this));

                this.$el.on('click', '.' + this.settings.printBtnClassName, function (e) {
                    e.preventDefault();
                    alert('Couldn\'t find a printer... Sorry :P');
                });

            }

        });

        $.fn[pluginName] = function (options) {
            return this.each(function () {
                if (!$.data(this, 'plugin_' + pluginName)) {
                        $.data( this, 'plugin_' + pluginName, new Plugin( this, options ) );
                }
            });
        };

})(jQuery, window, document);

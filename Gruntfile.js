
/*------------------------------------------------------------------------------

    Gruntfile

------------------------------------------------------------------------------*/

module.exports = function (grunt) {

    'use strict';

    grunt.initConfig({

        repo: {
            rootDir:    '.',
            srcDir:     '<%= repo.rootDir %>/src',
            distDir:    '<%= repo.rootDir %>/public/assets'
        },

        bowerjson:      grunt.file.readJSON('./bower.json'),
        packagejson:    grunt.file.readJSON('./package.json'),

        /*
         * Tasks
         */

        sass: {
            toDist: {
                files: {
                    '<%= repo.distDir %>/styles/content-card.css': '<%= repo.srcDir %>/styles/content-card.scss'
                },
                options: {
                    style: 'compressed',
                    noCache: true,
                    sourcemap: 'none'
                }
            }
        },

        uglify: {
            toDist: {
                files: {
                    '<%= repo.distDir %>/scripts/content-card.js': ['<%= repo.srcDir %>/scripts/content-card.js']
                },
                options: {
                    preserveComments: false
                }
            }
        },

        watch: {
            styles: {
                files: [
                    '<%= repo.srcDir %>/styles/**/*.scss'
                ],
                tasks: ['sass:toDist']
            },
            scripts: {
                files: [
                    '<%= repo.srcDir %>/scripts/**/*.js'
                ],
                tasks: ['uglify:toDist']
            }
        },

    });

    // Load task modules
    require('load-grunt-tasks')(grunt);

    // Register custom tasks
    grunt.registerTask('compile', ['sass:toDist', 'uglify:toDist']);

    // Register default task
    grunt.registerTask('default', ['sass:toDist', 'uglify:toDist', 'watch']);

};
